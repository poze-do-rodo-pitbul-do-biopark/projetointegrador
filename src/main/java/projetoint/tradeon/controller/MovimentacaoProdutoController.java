package projetoint.tradeon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projetoint.tradeon.domain.movimentacaoProduto.MovimentacaoProduto;
import projetoint.tradeon.domain.produto.Produto;
import projetoint.tradeon.repositories.MovimentacaoProdutoRepository;
import projetoint.tradeon.repositories.ProdutoRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/movimentacoes")
public class MovimentacaoProdutoController {

    @Autowired
    private MovimentacaoProdutoRepository movimentacaoProdutoRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    @GetMapping("/lista")
    public ResponseEntity<List<MovimentacaoProduto>> getmovimentacoes() {
        List<MovimentacaoProduto> movimentacoes = movimentacaoProdutoRepository.findAll();
        return ResponseEntity.ok(movimentacoes);
    };

    @PostMapping("/entrada")
    public ResponseEntity<List<MovimentacaoProduto>> entradaProdutos(
            @RequestBody MovimentacaoRequest request) {
        List<Integer> produtoIds = request.getProdutoIds();
        List<Integer> quantidades = request.getQuantidades();

        List<MovimentacaoProduto> movimentacoes = new ArrayList<>();
        if (produtoIds.size() != quantidades.size()) {
            return ResponseEntity.badRequest().body(null); // Tamanhos diferentes, erro
        }
        for (int i = 0; i < produtoIds.size(); i++) {
            int produtoId = produtoIds.get(i);
            int quantidade = quantidades.get(i);

            Optional<Produto> optionalProduto = produtoRepository.findById(produtoId);
            if (optionalProduto.isPresent()) {
                Produto produto = optionalProduto.get();
                produto.setQuant_estoque(produto.getQuant_estoque() + quantidade);
                produtoRepository.save(produto);

                MovimentacaoProduto movimentacao = new MovimentacaoProduto(produto, quantidade, MovimentacaoProduto.TipoMovimentacao.ENTRADA, new Date());
                movimentacaoProdutoRepository.save(movimentacao);
                movimentacoes.add(movimentacao);
            }
        }
        return ResponseEntity.ok(movimentacoes);
    }

    @PostMapping("/saida")
    public ResponseEntity<List<MovimentacaoProduto>> saidaProdutos(
            @RequestBody MovimentacaoRequest request) {
        List<Integer> produtoIds = request.getProdutoIds();
        List<Integer> quantidades = request.getQuantidades();

        List<MovimentacaoProduto> movimentacoes = new ArrayList<>();
        if (produtoIds.size() != quantidades.size()) {
            return ResponseEntity.badRequest().body(null); // Tamanhos diferentes, erro
        }
        for (int i = 0; i < produtoIds.size(); i++) {
            int produtoId = produtoIds.get(i);
            int quantidade = quantidades.get(i);

            Optional<Produto> optionalProduto = produtoRepository.findById(produtoId);
            if (optionalProduto.isPresent()) {
                Produto produto = optionalProduto.get();
                if (produto.getQuant_estoque() < quantidade) {
                    return ResponseEntity.badRequest().body(null); // Quantidade mínima excedida
                }
                produto.setQuant_estoque(produto.getQuant_estoque() - quantidade);
                produtoRepository.save(produto);

                MovimentacaoProduto movimentacao = new MovimentacaoProduto(produto, quantidade, MovimentacaoProduto.TipoMovimentacao.SAIDA, new Date());
                movimentacaoProdutoRepository.save(movimentacao);
                movimentacoes.add(movimentacao);
            }
        }
        return ResponseEntity.ok(movimentacoes);
    }

    @PostMapping("/venda")
    public ResponseEntity<List<MovimentacaoProduto>> vendaProdutos(
            @RequestBody MovimentacaoRequest request) {
        List<Integer> produtoIds = request.getProdutoIds();
        List<Integer> quantidades = request.getQuantidades();

        List<MovimentacaoProduto> movimentacoes = new ArrayList<>();
        if (produtoIds.size() != quantidades.size()) {
            return ResponseEntity.badRequest().body(null); // Tamanhos diferentes, erro
        }
        for (int i = 0; i < produtoIds.size(); i++) {
            int produtoId = produtoIds.get(i);
            int quantidade = quantidades.get(i);

            Optional<Produto> optionalProduto = produtoRepository.findById(produtoId);
            if (optionalProduto.isPresent()) {
                Produto produto = optionalProduto.get();
                if (produto.getQuant_estoque() < quantidade) {
                    return ResponseEntity.badRequest().body(null); // Quantidade mínima excedida
                }
                produto.setQuant_estoque(produto.getQuant_estoque() - quantidade);
                produtoRepository.save(produto);

                MovimentacaoProduto movimentacao = new MovimentacaoProduto(produto, quantidade, MovimentacaoProduto.TipoMovimentacao.VENDA, new Date());
                movimentacaoProdutoRepository.save(movimentacao);
                movimentacoes.add(movimentacao);
            }
        }
        return ResponseEntity.ok(movimentacoes);
    }
}

class MovimentacaoRequest {
    private List<Integer> produtoIds;
    private List<Integer> quantidades;

    public List<Integer> getProdutoIds() {
        return produtoIds;
    }

    public void setProdutoIds(List<Integer> produtoIds) {
        this.produtoIds = produtoIds;
    }

    public List<Integer> getQuantidades() {
        return quantidades;
    }

    public void setQuantidades(List<Integer> quantidades) {
        this.quantidades = quantidades;
    }
}
