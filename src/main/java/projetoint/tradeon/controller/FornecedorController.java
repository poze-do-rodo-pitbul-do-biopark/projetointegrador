package projetoint.tradeon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import projetoint.tradeon.domain.endereco.Endereco;
import projetoint.tradeon.domain.fornecedor.Fornecedor;
import projetoint.tradeon.records.RequestFornecedor;
import projetoint.tradeon.repositories.FornecedorRepository;
import projetoint.tradeon.repositories.endereco.EnderecoRepository;

import java.util.Optional;

@RestController
@RequestMapping("/fornecedor")
public class FornecedorController {
    @Autowired
    private FornecedorRepository fornecedorRepository;

    @Autowired
    private EnderecoRepository enderecoRepository;

    @GetMapping("/lista")
    public ResponseEntity getFornecedor() {
        var todosFornecedores = fornecedorRepository.findAll();
        return ResponseEntity.ok(todosFornecedores);
    }

    @GetMapping("/getEndereco/{id}")
    public ResponseEntity<Endereco> getEnderecoByFornecedorId(@PathVariable("id") Integer id) {
        Optional<Fornecedor> fornecedorOptional = fornecedorRepository.findById(id);
        if (fornecedorOptional.isPresent()) {
            Fornecedor fornecedor = fornecedorOptional.get();
            Endereco endereco = fornecedor.getEndereco();
            return ResponseEntity.ok(endereco);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/cadastro")
    public ResponseEntity<?> registrarFornecedor(@RequestBody @Validated RequestFornecedor data) {
        try {
            Optional<Endereco> optionalEndereco = enderecoRepository.findById(data.id_endereco());
            if (optionalEndereco.isPresent()) {
                Fornecedor newFornecedor = new Fornecedor(data);
                Endereco endereco = optionalEndereco.get();
                newFornecedor.setEndereco(endereco);
                System.out.println(data);
                Fornecedor savedFornecedor = fornecedorRepository.save(newFornecedor);
                return ResponseEntity.ok(savedFornecedor);
            } else {
                return ResponseEntity.badRequest().body("Endereço não encontrado");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erro ao processar a requisição");
        }
    }

    @PutMapping("/atualizar")
    public ResponseEntity updtFornecedor(@RequestBody @Validated RequestFornecedor data) {
        Optional<Fornecedor> optionalFornecedor = fornecedorRepository.findById(data.id_fornecedor());
        if (optionalFornecedor.isPresent()) {
            Fornecedor fornecedor = optionalFornecedor.get();
            fornecedor.setNome_fornecedor(data.nome_fornecedor());
            fornecedor.setCnpj_fornecedor(data.cnpj_fornecedor());
            fornecedor.setEmail_fornecedor(data.email_fornecedor());
            fornecedorRepository.save(fornecedor);
            return ResponseEntity.ok().body(fornecedor);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/cadastro/{id}")
    public ResponseEntity deleteFornecedor(@PathVariable Integer id) {
        Optional<Fornecedor> optionalFornecedor = fornecedorRepository.findById(id);
        if (optionalFornecedor.isPresent()) {
            Fornecedor fornecedor = optionalFornecedor.get();
            fornecedor.setActive(false);
            fornecedorRepository.save(fornecedor);
            return ResponseEntity.ok().body(fornecedor);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
