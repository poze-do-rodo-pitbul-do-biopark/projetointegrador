package projetoint.tradeon.controller;

import jakarta.annotation.Resource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebController {
    @GetMapping("/")
    public ClassPathResource homePage() {
        return new ClassPathResource("templates/Home.html");
    }
}
