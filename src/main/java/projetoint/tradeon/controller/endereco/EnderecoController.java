package projetoint.tradeon.controller.endereco;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import projetoint.tradeon.domain.endereco.Cidade;
import projetoint.tradeon.domain.endereco.Endereco;
import projetoint.tradeon.domain.endereco.Estado;
import projetoint.tradeon.domain.endereco.Pais;
import projetoint.tradeon.records.endereco.RequestEndereco;
import projetoint.tradeon.repositories.endereco.CidadeRepository;
import projetoint.tradeon.repositories.endereco.EnderecoRepository;
import projetoint.tradeon.repositories.endereco.EstadoRepository;
import projetoint.tradeon.repositories.endereco.PaisRepository;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/endereco")
public class EnderecoController {

    @Autowired
    private EnderecoRepository enderecoRepository;

    @Autowired
    private CidadeRepository cidadeRepository;

    @Autowired
    private EstadoRepository estadoRepository;

    @Autowired
    private PaisRepository paisRepository;


    @GetMapping("/lista")
    public ResponseEntity<List<Endereco>> getEnderecos() {
        List<Endereco> todosEnderecos = enderecoRepository.findAll();
        return ResponseEntity.ok(todosEnderecos);
    }



    @PostMapping("/cadastro")
    public ResponseEntity<?> registrarEndereco(@RequestBody @Validated RequestEndereco requestEndereco) {
        try {
            String rua = requestEndereco.rua();
            String numero = requestEndereco.numero();
            String bairro = requestEndereco.bairro();
            int cidadeId = requestEndereco.cidade_id();
            int estadoId = requestEndereco.estado_id();
            int paisId = requestEndereco.pais_id();

            Optional<Cidade> optionalCidade = cidadeRepository.findById(cidadeId);
            Optional<Estado> optionalEstado = estadoRepository.findById(estadoId);
            Optional<Pais> optionalPais = paisRepository.findById(paisId);

            if (optionalCidade.isPresent() && optionalEstado.isPresent() && optionalPais.isPresent()) {
                Endereco novoEndereco = new Endereco();
                novoEndereco.setRua(rua);
                novoEndereco.setNumero(numero);
                novoEndereco.setBairro(bairro);
                novoEndereco.setCidade(optionalCidade.get());
                novoEndereco.setEstado(optionalEstado.get());
                novoEndereco.setPais(optionalPais.get());
                Endereco savedEndereco = enderecoRepository.save(novoEndereco);
                return ResponseEntity.ok(savedEndereco);
            } else {
                return ResponseEntity.badRequest().body("Cidade, estado ou país não encontrados");
            }
        } catch (Exception e) {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erro ao processar a requisição");
        }
    }

    @PutMapping("/atualizar/{id}")
    public ResponseEntity<?> atualizarEndereco(@PathVariable Integer id, @RequestBody @Validated RequestEndereco requestEndereco) {
        Optional<Endereco> optionalEndereco = enderecoRepository.findById(id);
        if (optionalEndereco.isPresent()) {
            Endereco endereco = optionalEndereco.get();
            endereco.setRua(requestEndereco.rua());
            endereco.setNumero(requestEndereco.numero());
            endereco.setBairro(requestEndereco.bairro());
            enderecoRepository.save(endereco);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteEndereco(@PathVariable Integer id) {
        Optional<Endereco> optionalEndereco = enderecoRepository.findById(id);
        if (optionalEndereco.isPresent()) {
            enderecoRepository.deleteById(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}