package projetoint.tradeon.controller.endereco;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projetoint.tradeon.domain.endereco.Estado;
import projetoint.tradeon.repositories.endereco.EstadoRepository;

import java.util.List;

@RestController
@RequestMapping("/estado")
public class EstadoController {

    @Autowired
    private EstadoRepository estadoRepository;

    @GetMapping("/listar")
    @JsonIgnore
    public ResponseEntity<List<Estado>> listarEstados() {
        List<Estado> estados = estadoRepository.findAll();
        return ResponseEntity.ok(estados);
    }

    @PostMapping("/cadastrar")
    public ResponseEntity<Estado> cadastrarEstado(@RequestBody Estado estado) {
        Estado novoEstado = estadoRepository.save(estado);
        return ResponseEntity.ok(novoEstado);
    }

    @DeleteMapping("/remover/{id}")
    public ResponseEntity<Void> removerEstado(@PathVariable Integer id) {
        estadoRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}