package projetoint.tradeon.controller.endereco;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projetoint.tradeon.domain.endereco.Pais;
import projetoint.tradeon.repositories.endereco.PaisRepository;

import java.util.List;

@RestController
@RequestMapping("/pais")
public class PaisController {

    @Autowired
    private PaisRepository paisRepository;

    @GetMapping("/listar")
    public ResponseEntity<List<Pais>> listarPaises() {
        List<Pais> paises = paisRepository.findAll();
        return ResponseEntity.ok(paises);
    }

    @PostMapping("/cadastrar")
    public ResponseEntity<Pais> cadastrarPais(@RequestBody Pais pais) {
        Pais novoPais = paisRepository.save(pais);
        return ResponseEntity.ok(novoPais);
    }

    @DeleteMapping("/remover/{id}")
    public ResponseEntity<Void> removerPais(@PathVariable Integer id) {
        paisRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}