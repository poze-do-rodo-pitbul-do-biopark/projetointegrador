package projetoint.tradeon.controller.endereco;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projetoint.tradeon.domain.endereco.Cidade;
import projetoint.tradeon.repositories.endereco.CidadeRepository;

import java.util.List;

@RestController
@RequestMapping("/cidade")
public class CidadeController {

    @Autowired
    private CidadeRepository cidadeRepository;

    @GetMapping("/listar")
    @JsonIgnore
    public ResponseEntity<List<Cidade>> listarCidades() {
        List<Cidade> cidades = cidadeRepository.findAll();
        return ResponseEntity.ok(cidades);
    }

    @PostMapping("/cadastrar")
    public ResponseEntity<Cidade> cadastrarCidade(@RequestBody Cidade cidade) {
        Cidade novaCidade = cidadeRepository.save(cidade);
        return ResponseEntity.ok(novaCidade);
    }

    @DeleteMapping("/remover/{id}")
    public ResponseEntity<Void> removerCidade(@PathVariable Integer id) {
        cidadeRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}