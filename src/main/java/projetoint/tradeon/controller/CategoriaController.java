package projetoint.tradeon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import projetoint.tradeon.domain.categoria.Categoria;
import projetoint.tradeon.records.RequestCategoria;
import projetoint.tradeon.repositories.CategoriaRepository;

import java.util.Optional;

@RestController
@RequestMapping("/categoria")
public class CategoriaController {
    @Autowired
    private CategoriaRepository repository;

    @GetMapping("/lista")
    public ResponseEntity getCategoria() {
        var todasCategorias = repository.findAllByActiveTrue();
        return ResponseEntity.ok(todasCategorias);
    }

    @PostMapping("/cadastro")
    public ResponseEntity registrarCategoria(@RequestBody @Validated RequestCategoria data) {
        Categoria newCategoria = new Categoria(data);
        repository.save(newCategoria);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/atualizar")
    public ResponseEntity updtCategoria(@RequestBody @Validated RequestCategoria data) {
        Optional<Categoria> optionalCategoria = repository.findById(data.id_categoria());
        if (optionalCategoria.isPresent()) {
            Categoria categoria = optionalCategoria.get();
            categoria.setNome_categoria(data.nome_categoria());
            categoria.setDesc_categoria(data.desc_categoria());
            repository.save(categoria);
            return ResponseEntity.ok().body(categoria);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/cadastro/{id}")
    public ResponseEntity deleteCategoria(@PathVariable Integer id) {
        Optional<Categoria> optionalCategoria = repository.findById(id);
        if (optionalCategoria.isPresent()) {
            Categoria categoria = optionalCategoria.get();
            categoria.setActive(false);
            repository.save(categoria);
            return ResponseEntity.ok().body(categoria);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}