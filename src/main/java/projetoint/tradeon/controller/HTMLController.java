package projetoint.tradeon.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/paginas")
public class HTMLController {

    @GetMapping("/")
    public ModelAndView paginaPrincipal() {
        ModelAndView paginaPrincipal = new ModelAndView();
        paginaPrincipal.setViewName("Home");
        return paginaPrincipal;
    }

    @GetMapping("/login")
    public ModelAndView paginaLogin() {
        ModelAndView login = new ModelAndView();
        login.setViewName("Login");
        return login;
    }

    @GetMapping("/relatorios")
    public ModelAndView paginaRelatorios() {
        ModelAndView paginaRelatorios = new ModelAndView();
        paginaRelatorios.setViewName("Relatorios");
        return paginaRelatorios;
    }

    @GetMapping("/venda")
    public ModelAndView paginaVenda() {
        ModelAndView paginaVenda = new ModelAndView();
        paginaVenda.setViewName("Venda");
        return paginaVenda;
    }
    @GetMapping("/Home")
    public ModelAndView paginaHome() {
        ModelAndView paginaHome = new ModelAndView();
        paginaHome.setViewName("Home");
        return paginaHome;
    }
    @GetMapping("/saidaestoque")
    public ModelAndView paginaBaixaEstoque() {
        ModelAndView paginaBaixaEstoque = new ModelAndView();
        paginaBaixaEstoque.setViewName("Saida");
        return paginaBaixaEstoque;
    }
    @GetMapping("/entradaestoque")
    public ModelAndView paginaEntradaEstoque() {
        ModelAndView paginaEntradaEstoque = new ModelAndView();
        paginaEntradaEstoque.setViewName("EntradadeEstoque");
        return paginaEntradaEstoque;
    }

    @GetMapping("/fornecedor")
    public ModelAndView paginaCadastroFornecedor() {
        ModelAndView cadastroFornecedor = new ModelAndView();
        cadastroFornecedor.setViewName("Fornecedores");
        return cadastroFornecedor;
    }

    @GetMapping("/produto/lista")
    public ModelAndView paginaListaProduto() {
        ModelAndView listaProduto = new ModelAndView();
        listaProduto.setViewName("lista-produtos");
        return listaProduto;
    }

}