package projetoint.tradeon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import projetoint.tradeon.domain.categoria.Categoria;
import projetoint.tradeon.domain.fornecedor.Fornecedor;
import projetoint.tradeon.domain.produto.Produto;
import projetoint.tradeon.records.RequestProduto;
import projetoint.tradeon.repositories.CategoriaRepository;
import projetoint.tradeon.repositories.FornecedorRepository;
import projetoint.tradeon.repositories.ProdutoRepository;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/produto")
public class ProdutoController {
    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    private FornecedorRepository fornecedorRepository;

    @Autowired
    private CategoriaRepository categoriaRepository;

    @GetMapping("/lista")
    public ResponseEntity<List<Produto>> getProdutos() {
        List<Produto> todosProdutos = produtoRepository.findAllByActiveTrue();
        return ResponseEntity.ok(todosProdutos);
    }


    @PostMapping("/cadastro")
    public ResponseEntity<?> registrarProduto(@RequestBody @Validated RequestProduto data) {
        System.out.println("Produtos" + data);
        Optional<Fornecedor> optionalFornecedor = fornecedorRepository.findById(data.id_fornecedor());
        Optional<Categoria> optionalCategoria = categoriaRepository.findById(data.id_categoria());
        if (optionalFornecedor.isPresent()) {
            Produto newProduto = new Produto(data);
            newProduto.setFornecedor(optionalFornecedor.get());
            if (optionalCategoria.isPresent()) {
                newProduto.setCategoria(optionalCategoria.get());
                produtoRepository.save(newProduto);
                return ResponseEntity.ok().build();
            } else {
                return ResponseEntity.badRequest().body("Categoria invalida");
            }
        } else {
            return ResponseEntity.badRequest().body("Fornecedor não encontrado");
        }
    }

    @PutMapping("/atualizar")
    public ResponseEntity<?> updtProduto(@RequestBody @Validated RequestProduto data) {
        Optional<Produto> optionalProduto = produtoRepository.findById(data.id_produto());
        if (optionalProduto.isPresent()) {
            Optional<Fornecedor> optionalFornecedor = fornecedorRepository.findById(data.id_fornecedor());
            if (optionalFornecedor.isPresent()) {
                Produto produto = optionalProduto.get();
                produto.setNome_produto(data.nome_produto());
                produto.setDesc_produto(data.desc_produto());
                produto.setQuant_estoque(data.quant_estoque());
                produto.setUnidadeMedida(data.unidadeMedida());
                produto.setValor_venda(data.valor_venda());
                produto.setFornecedor(optionalFornecedor.get());
                produtoRepository.save(produto);
                return ResponseEntity.ok().body(produto);
            } else {
                return ResponseEntity.badRequest().body("Fornecedor não encontrado");
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/cadastro/{id}")
    public ResponseEntity<?> deleteProduto(@PathVariable Integer id) {
        Optional<Produto> optionalProduto = produtoRepository.findById(id);

        if (optionalProduto.isPresent()) {
            Produto produto = optionalProduto.get();
            produto.setActive(false);
            produtoRepository.save(produto);
            return ResponseEntity.ok().body(produto);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/lista-fornecedores")
    public ResponseEntity<List<Fornecedor>> getFornecedores() {
        List<Fornecedor> todosFornecedores = fornecedorRepository.findAll();
        return ResponseEntity.ok(todosFornecedores);
    }

    @GetMapping("/lista-categorias")
    public ResponseEntity<List<Categoria>> getCategoria() {
        List<Categoria> todasCategorias = categoriaRepository.findAllByActiveTrue();
        return ResponseEntity.ok(todasCategorias);
    }
}