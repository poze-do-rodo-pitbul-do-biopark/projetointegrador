package projetoint.tradeon.records;

import org.springframework.format.annotation.DateTimeFormat;
import projetoint.tradeon.domain.produto.Produto;

import java.util.Date;

public record RequestProduto(int id_produto,

                             String nome_produto,

                             String desc_produto,

                             int quant_estoque,

                             Produto.UnidadeMedida unidadeMedida,

                             Boolean perecivel,

                             @DateTimeFormat(pattern = "yyyy-MM-dd")
                             Date data_validade,

                             Float valor_venda,

                             int id_fornecedor,

                             int id_categoria) {
}
