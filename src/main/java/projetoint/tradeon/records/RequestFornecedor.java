package projetoint.tradeon.records;

public record RequestFornecedor(int id_fornecedor,

                  String nome_fornecedor,

                  String cnpj_fornecedor,

                  String email_fornecedor,

                  int id_endereco) {
}
