package projetoint.tradeon.records;

import java.util.Date;

public record RequestSaidaProdutos(
        Integer id_saida,
        Integer quantidade,
        Date data_saida,
        Boolean active,
        int id_produto) {
}
