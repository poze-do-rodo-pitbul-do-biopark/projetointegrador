package projetoint.tradeon.records.endereco;

public record RequestEndereco(int id_endereco,
                              String rua,
                              String numero,
                              String bairro,
                              int cidade_id,
                              int estado_id,
                              int pais_id
) {

}