package projetoint.tradeon.records.endereco;

public record RequestPais(int pais_id, String nome_pais) {
}