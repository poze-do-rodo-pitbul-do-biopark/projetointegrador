package projetoint.tradeon.records;

public record RequestCategoria(int id_categoria,

                               String nome_categoria,

                               String desc_categoria) {
}