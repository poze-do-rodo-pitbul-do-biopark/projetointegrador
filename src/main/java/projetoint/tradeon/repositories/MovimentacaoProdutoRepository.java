package projetoint.tradeon.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import projetoint.tradeon.domain.movimentacaoProduto.MovimentacaoProduto;

@Repository
public interface MovimentacaoProdutoRepository extends JpaRepository<MovimentacaoProduto, Integer> {
}
