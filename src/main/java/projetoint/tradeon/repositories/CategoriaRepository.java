package projetoint.tradeon.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import projetoint.tradeon.domain.categoria.Categoria;

import java.util.List;

public interface CategoriaRepository extends JpaRepository<Categoria, Integer> {
    List<Categoria> findAllByActiveTrue();
}
