package projetoint.tradeon.repositories.endereco;

import org.springframework.data.jpa.repository.JpaRepository;
import projetoint.tradeon.domain.endereco.Endereco;

public interface EnderecoRepository extends JpaRepository<Endereco, Integer> {
}
