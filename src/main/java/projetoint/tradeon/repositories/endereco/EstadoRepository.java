package projetoint.tradeon.repositories.endereco;


import org.springframework.data.jpa.repository.JpaRepository;
import projetoint.tradeon.domain.endereco.Estado;


public interface EstadoRepository extends JpaRepository<Estado, Integer> {
}
