package projetoint.tradeon.repositories.endereco;

import org.springframework.data.jpa.repository.JpaRepository;
import projetoint.tradeon.domain.endereco.Pais;

public interface PaisRepository extends JpaRepository<Pais, Integer> {
}
