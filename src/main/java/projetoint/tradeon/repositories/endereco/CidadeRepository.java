package projetoint.tradeon.repositories.endereco;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import projetoint.tradeon.domain.endereco.Cidade;

@Repository
public interface CidadeRepository extends JpaRepository<Cidade, Integer> {
}
