package projetoint.tradeon.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import projetoint.tradeon.domain.produto.Produto;

import java.util.List;

public interface ProdutoRepository extends JpaRepository<Produto, Integer> {
    List<Produto> findAllByActiveTrue();
}
