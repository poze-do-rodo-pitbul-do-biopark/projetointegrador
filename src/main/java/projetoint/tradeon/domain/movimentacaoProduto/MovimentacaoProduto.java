package projetoint.tradeon.domain.movimentacaoProduto;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import projetoint.tradeon.domain.produto.Produto;

import java.util.Date;

@Entity(name = "movimentacao_produto")
@Table(name = "movimentacao_produto")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id_movimentacao")
public class MovimentacaoProduto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_movimentacao")
    private int id_movimentacao;

    @ManyToOne
    @JoinColumn(name = "id_produto", nullable = false)
    private Produto produto;

    private int quantidade;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_movimentacao")
    private TipoMovimentacao tipoMovimentacao;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dataMovimentacao;

    public MovimentacaoProduto(Produto produto, int quantidade, TipoMovimentacao tipoMovimentacao, Date dataMovimentacao) {
        this.produto = produto;
        this.quantidade = quantidade;
        this.tipoMovimentacao = tipoMovimentacao;
        this.dataMovimentacao = dataMovimentacao;
    }

    public enum TipoMovimentacao {
        ENTRADA,
        SAIDA,
        VENDA
    }
}