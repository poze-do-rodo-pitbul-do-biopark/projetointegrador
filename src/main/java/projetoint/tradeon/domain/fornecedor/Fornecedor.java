package projetoint.tradeon.domain.fornecedor;

import jakarta.persistence.*;
import lombok.*;
import projetoint.tradeon.domain.endereco.Endereco;
import projetoint.tradeon.records.RequestFornecedor;


@Table (name = "fornecedor")
@Entity(name = "fornecedor")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id_fornecedor")
public class Fornecedor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_fornecedor")
    private int id_fornecedor;
    private String nome_fornecedor;
    private String cnpj_fornecedor;
    private String email_fornecedor;
    private Boolean active;
    @ManyToOne
    @JoinColumn(name = "id_endereco")
    private Endereco endereco;


    public Fornecedor(RequestFornecedor requestFornecedor) {
        this.nome_fornecedor = requestFornecedor.nome_fornecedor();
        this.cnpj_fornecedor = requestFornecedor.cnpj_fornecedor();
        this.email_fornecedor = requestFornecedor.email_fornecedor();
        this.active = true;
    }

}


