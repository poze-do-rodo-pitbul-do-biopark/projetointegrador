package projetoint.tradeon.domain.categoria;

import jakarta.persistence.*;
import lombok.*;
import projetoint.tradeon.records.RequestCategoria;

@Entity(name = "categoria")
@Table(name = "categoria")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id_categoria")
public class Categoria {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_categoria;
    private String nome_categoria;
    private String desc_categoria;
    private Boolean active;

    public Categoria(RequestCategoria requestCategoria) {
        this.nome_categoria = requestCategoria.nome_categoria();
        this.desc_categoria = requestCategoria.desc_categoria();
        this.active = true;
    }
}