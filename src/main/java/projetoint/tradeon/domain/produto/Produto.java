package projetoint.tradeon.domain.produto;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import projetoint.tradeon.domain.categoria.Categoria;
import projetoint.tradeon.domain.fornecedor.Fornecedor;
import projetoint.tradeon.records.RequestProduto;

import java.util.Date;

@Entity(name = "produto")
@Table(name = "produto")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id_produto")
public class Produto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_produto")
    private int id_produto;
    private String nome_produto;
    private String desc_produto;
    private Boolean perecivel;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date data_validade;
    private int quant_estoque;
    private UnidadeMedida unidadeMedida;
    private Float valor_venda;
    private Boolean active;

    @ManyToOne
    @JoinColumn(name = "id_fornecedor")
    private Fornecedor fornecedor;
    @ManyToOne
    @JoinColumn(name = "id_categoria")
    private Categoria categoria;

    public Produto(RequestProduto requestProduto) {
        this.nome_produto = requestProduto.nome_produto();
        this.desc_produto = requestProduto.desc_produto();
        this.perecivel = requestProduto.perecivel();
        this.data_validade = requestProduto.data_validade();
        this.quant_estoque = requestProduto.quant_estoque();
        this.unidadeMedida = requestProduto.unidadeMedida();
        this.valor_venda = requestProduto.valor_venda();
        this.active = true;
    }
    public enum UnidadeMedida {
        QUILOS,
        METROS,
        LITROS,
        MILIGRAMAS,
        UNIDADES,
        CAIXAS,

    }
}