package projetoint.tradeon.domain.endereco;

import jakarta.persistence.*;
import lombok.*;

@Entity(name = "endereco")
@Table(name = "endereco")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id_endereco")
public class Endereco {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_endereco")
    private int id_endereco;
    private String rua;
    private String numero;
    private String bairro;
    private Boolean active = true;

    @ManyToOne
    @JoinColumn(name = "cidade_id")
    private Cidade cidade;

    @ManyToOne
    @JoinColumn(name = "estado_id")
    private Estado estado;

    @ManyToOne
    @JoinColumn(name = "pais_id")
    private Pais pais;
}