package projetoint.tradeon.domain.endereco;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name="pais")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of="pais_id")
public class Pais {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pais_id")
    private int pais_id;

    private String nome_pais;

    @OneToMany(mappedBy = "pais", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Estado> estados;
}