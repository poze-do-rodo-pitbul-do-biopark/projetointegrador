package projetoint.tradeon.domain.endereco;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name="estado")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of="estado_id")
public class Estado {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "estado_id")
    private int estado_id;
    private String nome_estado;
    private String sigla;

    @ManyToOne
    @JoinColumn(name = "pais_id")
    @JsonIgnore
    private Pais pais;

    @OneToMany(mappedBy = "estado", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Cidade> cidades;

}