package projetoint.tradeon.domain.endereco;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "cidade")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "cidade_id")
public class Cidade {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cidade_id")
    private int cidade_id;
    private String nome_cidade;

    @ManyToOne
    @JoinColumn(name = "estado_id")
    private Estado estado;
}