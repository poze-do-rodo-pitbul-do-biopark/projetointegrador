package projetoint.tradeon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradeonApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradeonApplication.class, args);
	}

}
