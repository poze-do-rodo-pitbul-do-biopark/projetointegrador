function formatarCNPJ(cnpj) {
    cnpj = cnpj.replace(/\D/g, '');

    cnpj = cnpj.substring(0, 14);

    if (cnpj.length > 2) {
        cnpj = cnpj.replace(/^(\d{2})(\d)/, "$1.$2");
    }
    if (cnpj.length > 5) {
        cnpj = cnpj.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
    }
    if (cnpj.length > 8) {
        cnpj = cnpj.replace(/^(\d{2})\.(\d{3})\.(\d{3})(\d)/, "$1.$2.$3/$4");
    }
    if (cnpj.length > 12) {
        cnpj = cnpj.replace(/^(\d{2})\.(\d{3})\.(\d{3})\/(\d{4})(\d)/, "$1.$2.$3/$4-$5");
    }

    return cnpj;
}

document.getElementById('cnpj_fornecedor').addEventListener('input', function (e) {
    let input = e.target;
    let valor = input.value;
    valor = formatarCNPJ(valor);
    input.value = valor;
});


function limparCNPJ(cnpj) {
    return cnpj.replace(/\D/g, '');
}
