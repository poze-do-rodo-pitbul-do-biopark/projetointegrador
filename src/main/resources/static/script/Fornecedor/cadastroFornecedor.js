async function cadastrarFornecedor(enderecoId) {
    try {
        const form = document.getElementById('cadastroStartForm');
        if (!form) {
            throw new Error('Formulário não encontrado');
        }

        const formData = new FormData(form);
        const data = {};

        const camposObrigatorios = ['nome_fornecedor', 'cnpj_fornecedor', 'email_fornecedor'];
        for (const campo of camposObrigatorios) {
            const value = formData.get(campo).trim();
            if (!value) {
                alert('Por favor, preencha todos os campos obrigatórios.');
                return;
            }
            if (campo === 'cnpj_fornecedor') {
                data[campo] = limparCNPJ(value);
            } else {
                data[campo] = value;
            }
        }

        const email = data.email_fornecedor;
        if (!validateEmail(email)) {
            alert('Por favor, insira um e-mail válido.');
            return;
        }

        data.id_endereco = enderecoId;
        console.log(data);

        const submitButton = document.querySelector('.enviar');
        if (submitButton) {
            submitButton.disabled = true;
        }

        const response = await fetch('/fornecedor/cadastro', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        if (!response.ok) {
            throw new Error('Erro ao cadastrar fornecedor: ' + response.status);
        }

        console.log('Fornecedor cadastrado com sucesso!');
        alert('Fornecedor cadastrado com sucesso!');
        form.reset();
        window.location.reload();
    } catch (error) {
        console.error('Erro ao cadastrar fornecedor:', error);
        alert('Erro ao cadastrar fornecedor: ' + error.message);
    } finally {
        const submitButton = document.querySelector('.enviar');
        if (submitButton) {
            submitButton.disabled = false;
        }
    }
}

function validateEmail(email) {
    const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return re.test(String(email).toLowerCase());
}

function limparCNPJ(cnpj) {
    return cnpj.replace(/[^\d]+/g, '');
}
