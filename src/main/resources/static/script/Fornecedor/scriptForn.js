async function loadList() {
    const corpoTabela = document.getElementById('corpo-tabela');
    try {
        const response = await fetch('/fornecedor/lista', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (!response.ok) {
            throw new Error('Erro ao carregar fornecedores: ' + response.status);
        }

        const fornecedores = await response.json();

        console.log('Dados dos fornecedores:', fornecedores);

        if (fornecedores.length === 0) {
            console.warn('A resposta do servidor não contém dados de fornecedores.');
            return;
        }

        fornecedores.forEach(fornecedor => {
            console.log('Fornecedor:', fornecedor);

            const row = document.createElement('tr');

            const nomeFornecedorCell = document.createElement('td');
            nomeFornecedorCell.textContent = fornecedor.nome_fornecedor;
            row.appendChild(nomeFornecedorCell);

            const cnpjCell = document.createElement('td');
            cnpjCell.textContent = formatarCNPJ(fornecedor.cnpj_fornecedor)
            row.appendChild(cnpjCell);

            const emailFornecedorCell = document.createElement('td');
            emailFornecedorCell.textContent = fornecedor.email_fornecedor;
            row.appendChild(emailFornecedorCell);

            const enderecoCell = document.createElement('td');
            const button = document.createElement('button');
            button.textContent = 'Mostrar Endereço';
            button.classList.add('botao-endereco');
            button.onclick = function() {
                mostrarEndereco(fornecedor.id_fornecedor);
            };
            enderecoCell.appendChild(button);
            row.appendChild(enderecoCell);




            corpoTabela.appendChild(row);
        });
    } catch (error) {
        console.error('Erro ao carregar fornecedores:', error);
    }
}
window.addEventListener('DOMContentLoaded', loadList);
