async function cadastrarEndereco() {
    const enderecoData = {
        rua: document.getElementById('rua_endereco').value,
        numero: parseInt(document.getElementById('numero_endereco').value, 10) || 0,
        bairro: document.getElementById('bairro_endereco').value,
        cidade_id: document.getElementById('cidade_endereco').value,
        estado_id: document.getElementById('estado_endereco').value,
        pais_id: document.getElementById('pais_endereco').value
    };


    try {
        const response = await fetch('/endereco/cadastro', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(enderecoData)
        });

        if (!response.ok) {
            throw new Error('Erro ao cadastrar endereço: ' + response.status);
        }
        console.log('Endereço cadastrado com sucesso!');
        const endereco = await response.json();
        console.log(endereco)
        return endereco.id_endereco;

    } catch (error) {
        console.error('Erro ao cadastrar endereço:', error);
        alert('Erro ao cadastrar endereço');
        throw error;
    }
    console.log({id_endereco})
}
