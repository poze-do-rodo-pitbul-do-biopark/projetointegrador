async function mostrarEndereco(fornecedorId) {
    try {
        const response = await fetch(`/fornecedor/getEndereco/${fornecedorId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (!response.ok) {
            throw new Error('Erro ao carregar endereço');
        }

        const endereco = await response.json();
        const enderecoElement = document.getElementById('endereco');
        enderecoElement.textContent = `${endereco.rua}, ${endereco.numero}, ${endereco.bairro}, ${endereco.cidade.nome_cidade}, ${endereco.estado.sigla}, ${endereco.pais.nome_pais}`;

        const modal = document.getElementById('customModal');
        modal.classList.add('show');

        // Fechar o modal quando o usuário clicar fora dele
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.classList.remove('show');
            }
        };
    } catch (error) {
        console.error('Erro ao carregar endereço:', error);
    }
}

function setupEventListeners() {
    const closeFooterButton = document.querySelector('.custom-close-footer');
    if (closeFooterButton) {
        closeFooterButton.onclick = function () {
            const modal = document.getElementById('customModal');
            modal.classList.remove('show');
        };
    }
}

// Inicialize os eventos e a tabela
setupEventListeners();