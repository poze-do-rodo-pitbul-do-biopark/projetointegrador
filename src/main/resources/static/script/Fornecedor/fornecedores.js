document.addEventListener("DOMContentLoaded", function() {
    let fundoEscuro = document.getElementById("fundoEscuro");
    let formularioCadastro = document.getElementById("formulario-cadastro");

    function mostrarFormulario() {
        fundoEscuro.style.display = "flex";
    }

    function esconderFormulario() {
        fundoEscuro.style.display = "none";
    }

    document.getElementById("mostrar").addEventListener("click", mostrarFormulario);

    fundoEscuro.addEventListener("click", function(event) {
        if (event.target === fundoEscuro) {
            esconderFormulario();
        }
    });

    document.addEventListener("keydown", function(event) {
        if (event.key === "Escape") {
            esconderFormulario();
        }
    });

    document.addEventListener("click", function(event) {
        if (!formularioCadastro.contains(event.target) && event.target !== document.getElementById("mostrar")) {
            esconderFormulario();
        }
    });

    // Evento de clique no botão PESQUISAR
    document.querySelector(".button-pesquisar").addEventListener("click", function() {
        let filtro = document.getElementById("input-busca").value.toUpperCase();
        let tabela = document.getElementById("tabela-fornecedores");
        let linhas = tabela.getElementsByTagName("tr");

        for (let i = 0; i < linhas.length; i++) {
            let colunas = linhas[i].getElementsByTagName("td");
            let encontrado = false;

            for (let j = 0; j < colunas.length; j++) {
                let textoColuna = colunas[j].textContent || colunas[j].innerText;
                if (textoColuna.toUpperCase().indexOf(filtro) > -1) {
                    encontrado = true;
                    break;
                }
            }

            if (encontrado) {
                linhas[i].style.display = "";
            } else {
                linhas[i].style.display = "none";
            }
        }
    });
});
