const globalProdutos = []
async function getProdutos() {

    const produtoList = document.querySelector("#lista-produtos");
    const produtosResponse = await fetch('/produto/lista')

    const produtos = await produtosResponse.json()
    produtos.forEach((produto) => {
        const option = document.createElement('option');
        option.value = produto.id_produto;
        option.innerText = produto.nome_produto
        produtoList.appendChild(option);

    })
    globalProdutos.push(...produtos)
    console.log(produtos);
}
const produtosMovimentacao = []
const qtdMovimentacao = []

let movimentacao = {
    produtoIds: [],
    quantidades: []
};

async function inserirProduto() {
    const table = document.querySelector('#corpo-tabela');
    const selectedProduto = document.querySelector('#lista-produtos').value;
    const qtdProduto = document.querySelector('#quantidade').value;

    const selectedRow = table.querySelector(`tr[data-produto="${selectedProduto}"]`);

    if (selectedRow) {
        const cellQuantidade = selectedRow.querySelector('.quantidade');
        const novaQuantidade = parseInt(cellQuantidade.textContent) + parseInt(qtdProduto);
        cellQuantidade.textContent = novaQuantidade;

        const index = movimentacao.produtoIds.findIndex(id => id === selectedProduto);
        movimentacao.quantidades[index] = novaQuantidade;
    } else {
        // Se o produto não estiver na tabela, adicione uma nova linha
        const selectedProdutoData = globalProdutos.find(produto => produto.id_produto === parseInt(selectedProduto));

        if (selectedProdutoData) {
            const newRow = document.createElement('tr');
            newRow.setAttribute('data-produto', selectedProduto);

            const cellNomeProduto = document.createElement('td');
            const cellDescProduto = document.createElement('td');
            const cellQuantidade = document.createElement('td');
            const cellValor = document.createElement('td');
            cellQuantidade.classList.add('quantidade'); // Adicionar classe para facilitar a seleção posteriormente

            cellNomeProduto.textContent = selectedProdutoData.nome_produto;
            cellDescProduto.textContent = selectedProdutoData.desc_produto;
            cellQuantidade.textContent = qtdProduto;
            cellValor.textContent = selectedProdutoData.valor_venda;

            newRow.appendChild(cellNomeProduto);
            newRow.appendChild(cellDescProduto);
            newRow.appendChild(cellQuantidade);
            newRow.appendChild(cellValor);

            table.appendChild(newRow);

            movimentacao.produtoIds.push(selectedProduto);
            movimentacao.quantidades.push(parseInt(qtdProduto));
        }
    }

    console.log(movimentacao);
}

async function criarMovimentacao() {
    const data = JSON.stringify(movimentacao);
    const radioInputs = document.getElementsByName('movimentacao');

    let movimentacaoSelecionada;

    radioInputs.forEach(input => {
        if (input.checked) {
            movimentacaoSelecionada = input.value;
        }
    });

    movimentacaoSelecionada = "saida"

    if (movimentacaoSelecionada) {
        console.log('Tipo de movimentação selecionado:', movimentacaoSelecionada);
        if (movimentacaoSelecionada === 'entrada') {
            try {
                const res = await fetch('/movimentacoes/entrada', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: data
                })
            } catch (e) {
                console.error(e)
            }
        } else {
            try {
                const res = await fetch('/movimentacoes/saida', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: data
                })
            } catch (e) {
                console.log(e)
            }
        }
        alert("Movimentação cadastrada com sucesso");
        window.location.reload();
    } else {
        console.log('Nenhum tipo de movimentação selecionado');
    }
}


window.addEventListener('DOMContentLoaded', () => {
    getProdutos();
})