async function carregarPaises() {
    try {
        const response = await fetch('/pais/listar');
        const data = await response.json();

        const selectPais = document.getElementById('pais_endereco');
        selectPais.innerHTML = '';

        data.forEach(pais => {
            const option = document.createElement('option');
            option.value = pais.pais_id;
            option.textContent = pais.nome_pais;
            selectPais.appendChild(option);
        });
        // Chama carregarEstadosPorPais quando o dropdown de países for preenchido
        await carregarEstadosPorPais();
    } catch (error) {
        console.error('Erro ao carregar países:', error);
        alert('Erro ao carregar países');
    }
}

async function carregarEstadosPorPais() {
    const paisId = document.getElementById('pais_endereco').value;

    try {
        const response = await fetch(`/estado/listar?paisId=${paisId}`);
        const data = await response.json();

        const selectEstado = document.getElementById('estado_endereco');
        selectEstado.innerHTML = '';

        data.forEach(estado => {
            const option = document.createElement('option');
            option.value = estado.estado_id;
            option.textContent = estado.nome_estado;
            selectEstado.appendChild(option);
        });

        // Carregar as cidades baseado no estado selecionado inicialmente
        await carregarCidadesPorEstado();
    } catch (error) {
        console.error('Erro ao carregar estados:', error);
        alert('Erro ao carregar estados');
    }
}

async function carregarCidadesPorEstado() {
    const estadoId = document.getElementById('estado_endereco').value;

    try {
        const response = await fetch(`/cidade/listar?estadoId=${estadoId}`);
        const data = await response.json();

        const selectCidade = document.getElementById('cidade_endereco');
        selectCidade.innerHTML = '';

        // Filtrar as cidades com base no estado selecionado
        const cidadesFiltradas = data.filter(cidade => cidade.estado.estado_id === parseInt(estadoId));

        // Adicionar as opções das cidades filtradas ao dropdown
        cidadesFiltradas.forEach(cidade => {
            const option = document.createElement('option');
            option.value = cidade.cidade_id;
            option.textContent = cidade.nome_cidade;
            selectCidade.appendChild(option);
        });
    } catch (error) {
        console.error('Erro ao carregar cidades:', error);
        alert('Erro ao carregar cidades');
    }
}

// Event listener para chamar a função carregarCidadesPorEstado() quando o dropdown de estados for alterado
document.getElementById('estado_endereco').addEventListener('change', async function() {
    await carregarCidadesPorEstado();
});


// Carregar os países assim que a página for carregada
document.addEventListener('DOMContentLoaded', async () => {
    await carregarPaises();
});