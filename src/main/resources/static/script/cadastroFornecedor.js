async function cadastrarFornecedor(enderecoId) {
    try {
        const form = document.getElementById('cadastroFormForn');
        const formData = new FormData(form);
        const data = {};
        formData.forEach((value, key) => {
            if (key === 'cnpj_fornecedor') {
                data[key] = limparCNPJ(value);
            } else {
                data[key] = value;
            }
        });

        // Adiciona o ID do endereço aos dados do fornecedor
        data.id_endereco = enderecoId;
        console.log(data)
        const response = await fetch('/fornecedor/cadastro', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        if (!response.ok) {
            throw new Error('Erro ao cadastrar fornecedor: ' + response.status);
        }

        console.log('Fornecedor cadastrado com sucesso!');
        alert('Fornecedor cadastrado com sucesso!');
        // Limpar o formulário após o cadastro bem-sucedido
        form.reset();
    } catch (error) {
        console.error('Erro ao cadastrar fornecedor:', error);
        alert('Erro ao cadastrar fornecedor');
    }
}