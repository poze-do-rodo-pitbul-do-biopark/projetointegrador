function toggleCadastro() {
    console.log("clicou");
    const fundoEscuroProd = document.getElementById("fundoEscuroProd");
    const formularioCadastroProd = document.getElementById("cadastroFormProd");

    fundoEscuroProd.style.display === "none" ? fundoEscuroProd.style.display = "flex" : fundoEscuroProd.style.display = "none";
    formularioCadastroProd.style.display === "none" ? formularioCadastroProd.style.display = "flex" : formularioCadastroProd.style.display = "none";
}

async function registrarProduto(event) {
    event.preventDefault();

    const form = document.getElementById('cadastroFormProd');
    const formData = new FormData(form);
    const data = {};

    formData.forEach((value, key) => {
        if (key === 'perecivel') {
            data[key] = value === 'true';
        } else if (key === 'data_validade') {
            data[key] = value ? value : null;
        } else {
            data[key] = value;
        }
    });

    console.log('Dados do formulário:', data);

    try {
        const response = await fetch('/produto/cadastro', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        if (!response.ok) {
            throw new Error('Erro ao cadastrar produto: ' + response.status);
        }

        console.log('Produto cadastrado com sucesso!');
        form.reset();
        toggleDateField();
        window.location.reload();
    } catch (error) {
        console.error('Erro ao cadastrar produto:', error);
    }
}

async function carregarFornecedores() {
    const selectFornecedor = document.getElementById('id_fornecedor');
    try {
        const response = await fetch('/fornecedor/lista', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (!response.ok) {
            throw new Error('Erro ao carregar fornecedores: ' + response.status);
        }

        const data = await response.json();

        if (data.length === 0) {
            console.warn('A resposta do servidor não contém dados de fornecedores.');
            return;
        }

        data.forEach(fornecedor => {
            const option = document.createElement('option');
            option.value = fornecedor.id_fornecedor;
            option.textContent = fornecedor.nome_fornecedor;
            selectFornecedor.appendChild(option);
        });
    } catch (error) {
        console.error('Erro ao carregar fornecedores:', error);
    }
}

async function carregarCategoria() {
    const selectCategoria = document.getElementById('id_categoria');
    try {
        const response = await fetch('/categoria/lista', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (!response.ok) {
            throw new Error('Erro ao carregar categorias: ' + response.status);
        }

        const data = await response.json();

        if (data.length === 0) {
            console.warn('A resposta do servidor não contém dados de categorias.');
            const option = document.createElement('option');
            const produtoDiv = document.getElementById("categoria_input");
            option.value = null;
            option.textContent = "Sem categorias cadastradas";
            selectCategoria.appendChild(option);
            se.innerHTML += "<button>Cadastrar categoria</button>"
            return;
        }

        data.forEach(categoria => {
            const option = document.createElement('option');
            option.value = categoria.id_categoria;
            option.textContent = categoria.nome_categoria;
            selectCategoria.appendChild(option);
        });
    } catch (error) {
        console.error('Erro ao carregar categorias:', error);
    }
}

async function cadastrarCategoria() {
    const nome = document.querySelector("#nomeCategoria").value;
    const descricao = document.querySelector("#descricaoCategoria").value;

    const data = JSON.stringify({ nome_categoria: nome, desc_categoria: descricao })

    try {
        const res = await fetch("/categoria/cadastro", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: data,
        });
        await carregarCategoria();
    } catch (e) {
        console.error(e);
    }
}
const btnCategoria = document.querySelector("#btn-categoria");

btnCategoria.onclick = () => {
    cadastrarCategoria();
    carregarCategoria();
};

document.addEventListener('DOMContentLoaded', () => {
    const form = document.getElementById('cadastroFormProd');
    if (form) {
        form.addEventListener('submit', registrarProduto);
    }
    carregarFornecedores();
    carregarCategoria();
});
