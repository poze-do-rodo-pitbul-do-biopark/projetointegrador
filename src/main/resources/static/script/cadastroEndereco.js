async function cadastrarEndereco() {
    const enderecoData = {
        rua: document.getElementById('rua_endereco').value,
        numero: parseInt(document.getElementById('numero_endereco').value, 10) || 0,
        bairro: document.getElementById('bairro_endereco').value,
        cidade_id: document.getElementById('cidade_endereco').value,
        estado_id: document.getElementById('estado_endereco').value,
        pais_id: document.getElementById('pais_endereco').value
    };


    try {
        const response = await fetch('/endereco/cadastro', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(enderecoData)
        });

        if (!response.ok) {
            throw new Error('Erro ao cadastrar endereço: ' + response.status);
        }
        console.log('Endereço cadastrado com sucesso!');
        // Se necessário, você pode retornar o ID do endereço cadastrado para usar no cadastro do fornecedor
        const endereco = await response.json();
        console.log(endereco)
        return endereco.id_endereco; // Supondo que o endpoint retorne o ID do endereço

    } catch (error) {
        console.error('Erro ao cadastrar endereço:', error);
        alert('Erro ao cadastrar endereço');
        throw error; // Lançar o erro para que a função chamadora possa lidar com ele
    }
    console.log({id_endereco})
}
