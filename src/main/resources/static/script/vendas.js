function capturarDados() {
    const valor = document.getElementById("valor").value;
    const codigo = document.getElementById("codigo").value;
    const quantidade = document.getElementById("quantidade").value;

    const novoVenda = {
        valor: valor,
        codigo: codigo,
        quantidade: quantidade,
    };

    adicionarVenda(novoVenda);
}

function adicionarVenda(venda) {
    const novaLinha = document.createElement("tr");

    novaLinha.innerHTML = `
        <td>${venda.codigo}</td>
        <td></td>
        <td>${venda.quantidade}</td>
        <td>${venda.valor}</td>
        <td><button class="botao-excluir" onclick="excluirLinha(this)">Excluir</button></td>
    `;

    document.getElementById("filtragem-tabela").appendChild(novaLinha);
}

function excluirLinha(botao) {
    var linha = botao.parentNode.parentNode;
    linha.remove();
}

function calcularQntd() {
    var tabela = document.getElementById("filtragem-tabela");
    var totalQntd = 0;
    for (var i = 0; i < tabela.rows.length; i++) {
        totalQntd += parseInt(tabela.rows[i].cells[2].innerHTML);
    }
    alert("A quantidade total dos produtos é: " + totalQntd);
}

function calcularValor() {
    var tabela = document.getElementById("filtragem-tabela");
    var soma = 0;
    for (var i = 0; i < tabela.rows.length; i++) {
        soma += parseInt(tabela.rows[i].cells[3].innerHTML);
    }
    alert("O valor total dos produtos é: " + soma);
}