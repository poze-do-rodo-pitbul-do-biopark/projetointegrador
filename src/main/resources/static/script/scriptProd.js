async function loadList() {
    const corpoTabela = document.getElementById('corpo-tabela');
    try {
        const response = await fetch('/produto/lista', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (!response.ok) {
            throw new Error('Erro ao carregar produtos: ' + response.status);
        }

        const produtos = await response.json();

        console.log('Dados dos produtos:', produtos);

        if (produtos.length === 0) {
            console.warn('A resposta do servidor não contém dados de produtos.');
            return;
        }

        produtos.forEach(produto => {
            console.log('Produto:', produto);

            const row = document.createElement('tr');

            const nomeProdutoCell = document.createElement('td');
            nomeProdutoCell.textContent = produto.nome_produto;
            row.appendChild(nomeProdutoCell);

            const fornecedorCell = document.createElement('td');
            fornecedorCell.textContent = produto.fornecedor.nome_fornecedor;
            row.appendChild(fornecedorCell);

            const categoriaCell = document.createElement('td');
            categoriaCell.textContent = produto.categoria.nome_categoria;
            row.appendChild(categoriaCell);

            const quantidadeCell = document.createElement('td');
            quantidadeCell.textContent = produto.quant_estoque+" "+produto.unidadeMedida;
            row.appendChild(quantidadeCell);

            const valorCell = document.createElement('td');
            valorCell.textContent = produto.valor_venda;
            row.appendChild(valorCell);

            const perecivelCell = document.createElement('td');
            perecivelCell.textContent = produto.perecivel === true ? "Sim" : "Não";
            row.appendChild(perecivelCell)

            const validadeCell = document.createElement('td');
            if (produto.data_validade) {
                const data = new Date(produto.data_validade);
                var dataFormatada = data.toLocaleDateString("pt-BR");
            }
            validadeCell.textContent = produto.data_validade !== null ? dataFormatada :  "Produto não perecivel";
            row.appendChild(validadeCell);

            corpoTabela.appendChild(row);
        });
    } catch (error) {
        console.error('Erro ao carregar produtos:', error);
    }
}

window.addEventListener('DOMContentLoaded', loadList);


//Filtro

document.addEventListener('DOMContentLoaded', () => {
    const INPUT_BUSCA = document.getElementById('input-busca');
    const FILTRAR_TABELA = document.getElementById('corpo-tabela');

    INPUT_BUSCA.addEventListener('keyup', () => {
        let expressao = INPUT_BUSCA.value.toLowerCase();

        if (expressao.length === 1) {
            return;
        }

        let linhas = FILTRAR_TABELA.getElementsByTagName('tr');

        for (let posicao = 0; posicao < linhas.length; posicao++) {
            let conteudoDaLinha = linhas[posicao].innerHTML.toLowerCase();

            if (conteudoDaLinha.includes(expressao)) {
                linhas[posicao].style.display = '';
            } else {
                linhas[posicao].style.display = 'none';
            }
        }
    });
});

//Seletor
document.addEventListener('DOMContentLoaded', () => {
    const formulario = document.getElementById("lista-produtos");
    const fundoEscuro = document.createElement('div');
    fundoEscuro.id = "fundoEscuro"; // Definir ID para o fundo escuro
    document.body.appendChild(fundoEscuro); // Adicionar fundo escuro ao body

    const mostrarBotao = document.querySelector(".button-pesquisar");
    const inputBusca = document.getElementById('input-busca');
    const filtrarTabela = document.getElementById('corpo-tabela');

    mostrarBotao.addEventListener("click", () => {
        formulario.classList.toggle("visivel");
        fundoEscuro.style.display = "block";
    });

    inputBusca.addEventListener('keyup', () => {
        let expressao = inputBusca.value.toLowerCase();
        let linhas = filtrarTabela.getElementsByTagName('tr');

        for (let posicao = 0; posicao < linhas.length; posicao++) {
            let conteudoDaLinha = linhas[posicao].innerHTML.toLowerCase();

            if (conteudoDaLinha.includes(expressao)) {
                linhas[posicao].style.display = '';
            } else {
                linhas[posicao].style.display = 'none';
            }
        }
    });
});
