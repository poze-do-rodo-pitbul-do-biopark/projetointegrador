function toggleDateField() {
    const sim = document.getElementById('sim');
    const nao = document.getElementById('nao');
    const dateField = document.getElementById('data_validade');

    if (sim.checked) {
        dateField.style.display = 'block';
    } else {
        dateField.style.display = 'none';
    }
}

document.addEventListener('DOMContentLoaded', function () {
    const radios = document.getElementsByName('perecivel');
    for (let i = 0; i < radios.length; i++) {
        radios[i].addEventListener('change', toggleDateField);
    }
    toggleDateField();
});
