function formatarCNPJ(cnpj) {
    // Remove caracteres indesejados
    cnpj = cnpj.replace(/\D/g, '');

    // Limita o tamanho a 14 caracteres (máximo para CNPJ)
    cnpj = cnpj.substring(0, 14);

    // Aplica a máscara de formatação
    // Primeiro bloco de 2 dígitos
    if (cnpj.length > 2) {
        cnpj = cnpj.replace(/^(\d{2})(\d)/, "$1.$2");
    }
    // Segundo bloco de 3 dígitos
    if (cnpj.length > 5) {
        cnpj = cnpj.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
    }
    // Terceiro bloco de 3 dígitos
    if (cnpj.length > 8) {
        cnpj = cnpj.replace(/^(\d{2})\.(\d{3})\.(\d{3})(\d)/, "$1.$2.$3/$4");
    }
    // Bloco de 4 dígitos e os 2 dígitos verificadores
    if (cnpj.length > 12) {
        cnpj = cnpj.replace(/^(\d{2})\.(\d{3})\.(\d{3})\/(\d{4})(\d)/, "$1.$2.$3/$4-$5");
    }

    return cnpj;
}

document.getElementById('cnpj_fornecedor').addEventListener('input', function (e) {
    let input = e.target;
    let valor = input.value;
    valor = formatarCNPJ(valor);
    input.value = valor;
});


function limparCNPJ(cnpj) {
    return cnpj.replace(/\D/g, ''); // Remove tudo o que não é dígito
}
