function capturarDados() {

  const novoSaida = {
    valor: valor,
    codigo: codigo,
    quantidade: quantidade,
  };

  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: "/saida/registrar",
    data: JSON.stringify(novaSaida),
    success: function (response) {
      console.log("Saída de produto registrada com sucesso:", response);
    },
    error: function (e) {
      console.error("Erro ao registrar saída de produto:", e);
    }
  });

  const valor = document.getElementById("valor").value;
  const codigo = document.getElementById("codigo").value;
  const quantidade = document.getElementById("quantidade").value;

  adicionarSaida(novoSaida);



}

function adicionarSaida(saida) {
  const novaLinha = document.createElement("tr");

  novaLinha.innerHTML = `
        <td>${saida.codigo}</td>
        <td></td>
        <td>${saida.quantidade}</td>
        <td>${saida.valor}</td>
        <td><button class="botao-excluir" onclick="excluirLinha(this)"></button></td>
        `;

  // Adicionar a nova linha à tabela
  document.getElementById("filtragem-tabela").appendChild(novaLinha);
}

function excluirLinha(botao) {
  var linha = botao.parentNode.parentNode;
  linha.remove();
}


function calcularQntd(){

  var tabela = document.getElementById("filtragem-tabela");
  var totalQntd = 0;
  for (var i = 0; i < tabela.rows.length; i++) {
    totalQntd += parseInt(tabela.rows[i].cells[2].innerHTML);
  }
  alert("O valor total dos produtos é: " + totalQntd);

}

function calcularValor() {
  var tabela = document.getElementById("filtragem-tabela");
  var soma = 0;
  for (var i = 0; i < tabela.rows.length; i++) {
    soma += parseInt(tabela.rows[i].cells[3].innerHTML);
  }
  alert("O valor total dos produtos é: " + soma);
}