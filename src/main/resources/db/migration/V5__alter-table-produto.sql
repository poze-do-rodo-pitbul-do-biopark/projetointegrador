ALTER TABLE produto
    ADD COLUMN id_categoria INT,
    ADD CONSTRAINT fk_categoria_produto FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);
