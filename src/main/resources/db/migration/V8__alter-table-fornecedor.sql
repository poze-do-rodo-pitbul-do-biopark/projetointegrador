ALTER TABLE fornecedor
    ADD COLUMN id_endereco INT,
    ADD FOREIGN KEY (id_endereco) REFERENCES endereco(id_endereco);
