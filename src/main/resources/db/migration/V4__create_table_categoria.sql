CREATE TABLE categoria
(
    id_categoria    INT AUTO_INCREMENT PRIMARY KEY,
    nome_categoria  VARCHAR(255) NOT NULL,
    desc_categoria VARCHAR(500) NOT NULL,
    active BOOLEAN
);