CREATE TABLE produto_fornecedor
(
    id            INT AUTO_INCREMENT PRIMARY KEY,
    id_produto    INT,
    id_fornecedor INT,
    FOREIGN KEY (id_produto) REFERENCES produto (id_produto),
    FOREIGN KEY (id_fornecedor) REFERENCES fornecedor (id_fornecedor)
);