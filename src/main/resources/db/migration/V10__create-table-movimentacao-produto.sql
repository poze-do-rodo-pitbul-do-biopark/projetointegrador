
CREATE TABLE movimentacao_produto (
                                      id_movimentacao INT AUTO_INCREMENT PRIMARY KEY,
                                      id_produto INT NOT NULL,
                                      quantidade INT NOT NULL,
                                      tipo_movimentacao VARCHAR(255) NOT NULL,
                                      data_movimentacao DATE NOT NULL,
                                      FOREIGN KEY (id_produto) REFERENCES produto(id_produto)
);