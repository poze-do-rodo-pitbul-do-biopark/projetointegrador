CREATE TABLE pais (
                      pais_id INT AUTO_INCREMENT PRIMARY KEY,
                      nome_pais VARCHAR(255) NOT NULL
);
CREATE TABLE estado (
                        estado_id INT AUTO_INCREMENT PRIMARY KEY,
                        nome_estado VARCHAR(255) NOT NULL,
                        sigla CHAR(2) NOT NULL,
                        pais_id INT,
                        FOREIGN KEY (pais_id) REFERENCES pais(pais_id)
);
CREATE TABLE cidade (
                        cidade_id INT AUTO_INCREMENT PRIMARY KEY,
                        nome_cidade VARCHAR(255) NOT NULL,
                        estado_id INT,
                        FOREIGN KEY (estado_id) REFERENCES estado(estado_id)
);
