CREATE TABLE produto
(
    id_produto   INT AUTO_INCREMENT PRIMARY KEY,
    nome_produto VARCHAR(255) NOT NULL,
    desc_produto VARCHAR(500) NOT NULL,
    quant_estoque    INT          NOT NULL,
    unidade_medida VARCHAR(50),
    perecivel BOOLEAN,
    data_validade DATE,
    valor_venda  DOUBLE       NOT NULL,
    id_fornecedor INT,
    active BOOLEAN,
    FOREIGN KEY (id_fornecedor) REFERENCES fornecedor(id_fornecedor)
);