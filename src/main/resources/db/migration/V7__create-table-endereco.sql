CREATE TABLE endereco (
                          id_endereco INT AUTO_INCREMENT PRIMARY KEY,
                          rua VARCHAR(255),
                          numero INT,
                          bairro VARCHAR(255),
                          cidade_id INT,
                          estado_id INT,
                          pais_id INT,

                          active BOOLEAN,
                          FOREIGN KEY (cidade_id) REFERENCES cidade(cidade_id),
                          FOREIGN KEY (estado_id) REFERENCES estado(estado_id),
                          FOREIGN KEY (pais_id) REFERENCES pais(pais_id)
);
